FROM python:3.6-alpine3.7
#FROM python:3-onbuild

ENV PACKAGES="\
    libxml2-dev\
    libxslt-dev\
    python-dev\
    dumb-init \
    musl \
    libc6-compat \
    linux-headers \
    build-base \
    bash \
    tree\
    ca-certificates \
    freetype \
    libgfortran \
    libgcc \
    libstdc++ \
    openblas \
    tcl \
    tk \
    libssl1.0"

# update apk repo
RUN echo "http://dl-4.alpinelinux.org/alpine/v3.7/main" >> /etc/apk/repositories && \
    echo "http://dl-4.alpinelinux.org/alpine/v3.7/community" >> /etc/apk/repositories

RUN apk add --no-cache --virtual build-dependencies $PACKAGES

WORKDIR /home

RUN apk update

#ENV PYTHONUNBUFFERED 1

# COPY requirements.txt /home/
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools

RUN apk update \
  && apk add --virtual .build-deps gcc python3-dev musl-dev libffi-dev \
  # TODO workaround start
  && apk del libressl-dev \
  && apk add openssl-dev \
  && pip install cryptography==2.2.2 \
  && apk del openssl-dev \
  && apk add libressl-dev

COPY . /home

# Add requirements.txt file to container
COPY djangorequirements.txt /home

RUN pip install -r /home/djangorequirements.txt


#CMD ["python", "VeilleSante/manage.py", "runserver", "0.0.0.0:5556"]
