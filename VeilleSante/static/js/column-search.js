"use strict";
var KTDatatablesSearchOptionsColumnSearch = function() {

	$.fn.dataTable.Api.register('column().title()', function() {
		return $(this.header()).text().trim();
	});

	var initTable1 = function() {

		// begin first table
		var table = $('#kt_table_1').DataTable({
			responsive: true,

			// Pagination settings

			// read more: https://datatables.net/examples/basic_init/dom.html

			ajax: {
				url: 'http://apisante.nextalert.net/v1.0.1/article/last',
				type: 'POST',
				data: {
					// parameters  custom backend script demo
					columnsDef: [
						'id_article', 'title', 'published', 'summary',
						'link_site', 'date_creation', 'Actions',],
				},
			},
			columns: [
				{data: 'id_article'},
				{data: 'title'},
				{data: 'published', type: "date"},
				{data: 'summary',
					"render": function (val, type, row) {
	                    return '<button type="button" class="btn btn-outline-brand btn-sm" data-toggle="modal" data-target="#kt_modal_' + row['id_article'] + '">' + val.slice(0, 350) + '...' + '</button>' +
	                    `<!--begin::Modal-->
							<div class="modal fade" id="kt_modal_` + row['id_article'] + `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">` + `<a class="kt-link kt-font-bold" href="` + row['link_site'] + `" target="_blank">Article Entier</a>` + `</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<p>
												` + row['summary_internal'] + `
											</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
										</div>
									</div>
								</div>
							</div>

							<!--end::Modal-->`;
	                }
				},
				{data: 'link_site',
					"render": function (val, type, row) {
	                    return '<a class="kt-link kt-font-bold" href="' + val + '" target="_blank">' + val.slice(0, 150) + '...' + '</a>';
	                }
				},
				{data: 'date_creation'},
				{data: 'Actions', responsivePriority: -1},
			],
			initComplete: function() {
				var thisTable = this;
				var rowFilter = $('<tr class="filter"></tr>').appendTo($(table.table().header()));

				this.api().columns().every(function() {
					var column = this;
					var input;

					switch (column.title()) {
						case 'id_article':
						case 'link_site':
						case 'summary':
							input = $(`<input type="text" class="form-control form-control-sm form-filter kt-input" data-col-index="` + column.index() + `"/>`);
							break;
						case 'title':
							input = $(`<input type="text" class="form-control form-control-sm form-filter kt-input" data-col-index="` + column.index() + `"/>`);
							break;

						case 'published':
							input = $(`
							<div class="input-group date">
								<input type="text" class="form-control form-control-sm kt-input" readonly placeholder="From" id="kt_datepicker_1"
								 data-col-index="` + column.index() + `"/>
								<div class="input-group-append">
									<span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
								</div>
							</div>
							<div class="input-group date">
								<input type="text" class="form-control form-control-sm kt-input" readonly placeholder="To" id="kt_datepicker_2"
								 data-col-index="` + column.index() + `"/>
								<div class="input-group-append">
									<span class="input-group-text"><i class="la la-calendar-o glyphicon-th"></i></span>
								</div>
							</div>`);
							break;

						case 'Actions':
							var search = $(`<button class="btn btn-brand kt-btn btn-sm kt-btn--icon">
							  <span>
							    <i class="la la-search"></i>
							    <span>Search</span>
							  </span>
							</button>`);

							var reset = $(`<button class="btn btn-secondary kt-btn btn-sm kt-btn--icon">
							  <span>
							    <i class="la la-close"></i>
							    <span>Reset</span>
							  </span>
							</button>`);

							$('<th>').append(search).append(reset).appendTo(rowFilter);

							$(search).on('click', function(e) {
								e.preventDefault();
								var params = {};
								$(rowFilter).find('.kt-input').each(function() {
									var i = $(this).data('col-index');
									console.log(i)
									if (params[i]) {
										params[i] += '|' + $(this).val();
									}
									else {
										params[i] = $(this).val();
									}
								});
								$.each(params, function(i, val) {
									// apply search params to datatable
									table.column(i).search(val ? val : '', false, false);
								});
								table.table().draw();
							});

							$(reset).on('click', function(e) {
								e.preventDefault();
								$(rowFilter).find('.kt-input').each(function(i) {
									$(this).val('');
									table.column($(this).data('col-index')).search('', false, false);
								});
								table.table().draw();
							});
							break;
					}

					if (column.title() !== 'Actions') {
						$(input).appendTo($('<th>').appendTo(rowFilter));
					}
				});

				 // hide search column for responsive table
				 var hideSearchColumnResponsive = function () {
		           thisTable.api().columns().every(function () {
			           var column = this
			           if(column.responsiveHidden()) {
				           $(rowFilter).find('th').eq(column.index()).show();
			           } else {
				           $(rowFilter).find('th').eq(column.index()).hide();
			           }
           })
         };

				// init on datatable load
				hideSearchColumnResponsive();
				// recheck on window resize
				window.onresize = hideSearchColumnResponsive;

				$('#kt_datepicker_1,#kt_datepicker_2').datetimepicker();
			},
			columnDefs: [
				{
					targets: 1,
					title: 'title',
					width: '15px',
				},
				{
					targets: 2,
					width: '185px',
				},
				{
					targets: 3,
					type: "date",
				},
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
                                <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                            </div>
                        </span>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>`;
					},
				},
				{
					targets: 4,
					orderable: true,
				},
				{
					targets: 5,
					width: '95px',
				},
			],
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesSearchOptionsColumnSearch.init();
});
